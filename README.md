# GatewayPortal

This project was generated with 
- Angular CLI version 11.2.2.
- Node v10.24.0
- Npm v10.24.0

## Prequisits
- Node
- Npm
- Angular CLI

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
