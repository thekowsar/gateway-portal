import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  addExtraClass = false;
  constructor(public snackBar: MatSnackBar, private zone: NgZone, private dialog: MatDialog) {

  }

  showSuccess(message: string): void {
    this.zone.run(() => {
      const config = new MatSnackBarConfig();
      config.horizontalPosition = this.horizontalPosition;
      config.verticalPosition = this.verticalPosition;
      config.panelClass = ['isa_success'];
      config.duration = 5000;
      this.snackBar.open(message, 'X', config);
    });
  }

  showError(msg: any): void {
    const msgup = msg.error ? msg.message : msg;
    this.zone.run(() => {
      const config = new MatSnackBarConfig();
      config.horizontalPosition = this.horizontalPosition;
      config.verticalPosition = this.verticalPosition;
      config.panelClass = ['isa_error'];
      config.duration = 5000;
      this.snackBar.open(msg, 'X', config);
    
    });
  }
}
