import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { GatewayApi } from '../constant/api-config';

export class Gateway{
  constructor(
    public gatewayOid:string,
    public serialNumber:string,
    public gatewayName:string,
    public ipv4Address:string,
    public peripheralDevices: PeripheralDevice[],
  ){}
}

export class PeripheralDevice {
  constructor(
    public peripheralDeviceOid:string,
    public uid:String,
    public vendor:string,
    public status:string,
    public gatewayOid:string,
    public createdOn:string
  ){}
}

export class GatewayCreate{
  constructor(
    public serialNumber:string,
    public gatewayName:string,
    public ipv4Address:string,
  ){}
}

export class DeviceCreate{
  constructor(
    public vendor:string,
    public status:string,
    public gatewayOid:string,
  ){}
}

export class GatewayResponse{
  constructor(
    public data:Gateway[],
    public count:number,
    public userMessage:string,
  ){}
}

export class GatewayByOidResponse{
  constructor(
    public obj:Gateway,
    public userMessage:string
  ){}
}

export class DeviceDeletedResponse{
  constructor(
    public obj:String,
    public userMessage:string
  ){}
}

@Injectable({
  providedIn: 'root'
})
export class HttpclientService {

  constructor(
    private httpClient:HttpClient
  ) { }

  getGateways(offset:string, limit:string){
    let params = new HttpParams()
    .set('offset', offset)
    .set('limit', limit);
    return this.httpClient.get<GatewayResponse>(GatewayApi.GATEWAY_GET_ALL_URL, {params});
  }

  createGateway(gatewayCreate:GatewayCreate){
    return this.httpClient.post<Gateway>(GatewayApi.GATEWAY_SAVE_URL, gatewayCreate);
  }

  getGatewayByOid(oid:String){
    return this.httpClient.get<GatewayByOidResponse>(GatewayApi.GATEWAY_GET_BY_OID_URL + oid);
  }

  createDevice(deviceCreate:DeviceCreate){
    return this.httpClient.post<Gateway>(GatewayApi.DEVICE_SAVE_URL, deviceCreate);
  }

  deleteDevice(oid:String){
    return this.httpClient.delete<DeviceDeletedResponse>(GatewayApi.DEVICE_DELETE_URL + oid);
  }
}
