import { environment } from 'src/environments/environment';

export class APIConfig {
    static BASE_API_URL = environment.baseUrl + environment.port;
}

export class GatewayApi {
    static GATEWAY_GET_ALL_URL = APIConfig.BASE_API_URL + '/v1/gateway/get-all';
    static GATEWAY_SAVE_URL = APIConfig.BASE_API_URL + '/v1/gateway/save';
    static GATEWAY_GET_BY_OID_URL = APIConfig.BASE_API_URL + '/v1/gateway/get-by-oid/';
    static DEVICE_SAVE_URL = APIConfig.BASE_API_URL + '/v1/peripheral/device/save';
    static DEVICE_DELETE_URL = APIConfig.BASE_API_URL + '/v1/peripheral/device/delete/';
}

