import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDeviceListComponent } from './dialog-device-list.component';

describe('DialogDeviceListComponent', () => {
  let component: DialogDeviceListComponent;
  let fixture: ComponentFixture<DialogDeviceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogDeviceListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDeviceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
