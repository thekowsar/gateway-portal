import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GatewayComponent } from '../gateway/gateway.component';
import {MatTableDataSource} from '@angular/material/table';
import { Gateway } from '../service/httpclient.service';

@Component({
  selector: 'app-dialog-device-list',
  templateUrl: './dialog-device-list.component.html',
  styleUrls: ['./dialog-device-list.component.css']
})
export class DialogDeviceListComponent implements OnInit {

  displayedColumns: string[] = ['uid', 'vendor', 'status', 'createdOn'];
  dataSource =  new MatTableDataSource<any>();

  constructor(
    public dialogRef: MatDialogRef<GatewayComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Gateway) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

}
