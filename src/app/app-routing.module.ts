import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddGatewayComponent } from './add-gateway/add-gateway.component';
import { GatewayDetailComponent } from './gateway-detail/gateway-detail.component';
import { GatewayComponent } from './gateway/gateway.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  {path:'', redirectTo: "/gateways", pathMatch: 'full'},
  {path: "gateways", component: GatewayComponent},
  {path: "gateways/:id", component: GatewayDetailComponent},
  {path: "addgateway", component: AddGatewayComponent},
  {path: "**", component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
